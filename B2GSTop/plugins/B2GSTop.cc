// -*- C++; tab-width:4 -*-
//
// Package:    B2GSTop/B2GSTop
// Class:      B2GSTop
//
/**\class B2GSTop B2GSTop.cc B2GSTop/B2GSTop/plugins/B2GSTop.cc

   Description: [one line class summary]

   Implementation:
   [Notes on implementation]
*/
//
// Original Author:  Ivan Razumov
//         Created:  Thu, 12 Nov 2015 12:23:14 GMT
//
//


// system include files
#include <memory>
#include <vector>
#include <iostream>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

// ROOT
#include "TLorentzVector.h"
#include "TTree.h"
#include "TFile.h"

//
// class declaration
//

#define get(module, label, index) _get(iEvent, module, label, index)
typedef std::pair<std::string, std::string> dkey;
typedef std::map<dkey, std::vector<float>>  dmap;
typedef dmap::iterator diter;

using std::vector;

class B2GSTop : public edm::EDAnalyzer
{
public:
	explicit B2GSTop(const edm::ParameterSet&);
	~B2GSTop();

	static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
	virtual void beginJob() override;
	virtual void analyze(const edm::Event&, const edm::EventSetup&) override;
	virtual void endJob() override;

	//virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
	//virtual void endRun(edm::Run const&, edm::EventSetup const&) override;
	//virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
	//virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
    void produceLV(const edm::Event& iEvent, char const *label, char const* instPrefix,
						vector<TLorentzVector> * out);

	// ----------member data ---------------------------
	vector<TLorentzVector> *jets;
    vector<TLorentzVector> *muons;
    vector<TLorentzVector> *electrons;

	// ---------- output -------------------------------
	TTree* outTree;
    TFile* outF;

   // ----------- Temporary storage --------------------
	dmap _data;
	float _get(const edm::Event& iEvent, std::string module, std::string label, size_t index);
};

//
// constants, enums and typedefs
//

//
// static data member definitions
//

//
// constructors and destructor
//
B2GSTop::B2GSTop(const edm::ParameterSet& iConfig)
{
    //now do what ever initialization is needed
//    JetCollection = iConfig.getParameter<std::string>("JetCollection");
    outF = new TFile("B2GSTop.root", "RECREATE");
    outF->cd();

	jets = new std::vector<TLorentzVector>();
	muons = new std::vector<TLorentzVector>();
	electrons = new std::vector<TLorentzVector>();

	outTree = new TTree("DIG", "Dmitri-Ivan-Grigori");
	outTree->Branch("muons", &muons, 32000, 0);
	outTree->Branch("jets", &jets, 32000, 0);
	outTree->Branch("electrons", &electrons, 32000, 0);
}


B2GSTop::~B2GSTop()
{
    // do anything here that needs to be done at desctruction time
    // (e.g. close files, deallocate resources etc.)
    outF->cd();
    outTree->Write();
    outF->Close();
}


//
// member functions
//

// ------------ method called for each event  ------------
void
B2GSTop::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup)
{
    muons->clear();
    jets->clear();
    electrons->clear();
	_data.clear();

    using namespace edm;
    std::vector<TLorentzVector> allJets, allMuons, allElectrons;
	produceLV(iEvent, "jetsAK4", "jetAK4", &allJets);
	produceLV(iEvent, "muons", "mu", &allMuons);
	produceLV(iEvent, "electrons", "el", &allElectrons);

	for (size_t iMu = 0; iMu < allMuons.size(); iMu++)
    {
		if (!get("muons", "muIsPFMuon", iMu))
			continue;

		if (!get("muons", "muIsGlobalMuon", iMu))
			continue;

		if (get("muons", "muPt", iMu) <= 26)
			continue;

		if (abs(get("muons", "muEta", iMu)) >= 2.1)
			continue;

		// Muon chi**2 cut omitted

		if (get("muons", "muNumberTrackerLayers", iMu) <= 5)
			continue;

		if (get("muons", "muNumberValidMuonHits", iMu) < 1)
			continue;

		if (get("muons", "muD0", iMu) >= 0.2)
			continue;

		if (get("muons", "muDz", iMu) >= 0.5)
			continue;

		if (get("muons", "muNumberValidPixelHits", iMu) < 1)
			continue;

		if (get("muons", "muNumberMatchedStations", iMu) < 2)
			continue;

		if (get("muons", "muIso04", iMu) > 0.15)
			continue;

		muons->push_back(allMuons.at(iMu));
    }

	// Electrons
    for (size_t iEl = 0; iEl < allElectrons.size(); iEl++)
    {
		electrons->push_back(allElectrons.at(iEl));
	}

	//Jets
	for (size_t iJet = 0; iJet< allJets.size(); iJet++)
	{
		if (get("jetsAK4", "jetAK4Pt", iJet) <= 40)
			continue;

		if (abs(get("jetsAK4", "jetAK4Eta", iJet))>= 4.7)
			continue;

		jets->push_back(allJets.at(iJet));
	}

    outTree->Fill();
}

void B2GSTop::produceLV(const edm::Event& iEvent, char const *label, char const* instPrefix,
						vector<TLorentzVector>* out)
{
	using namespace edm;
	Handle<vector<float>> Pt, Phi, Eta, E;

	iEvent.getByLabel(label, std::string(instPrefix).append("Pt"), Pt);
	iEvent.getByLabel(label, std::string(instPrefix).append("Phi"), Phi);
	iEvent.getByLabel(label, std::string(instPrefix).append("Eta"), Eta);
	iEvent.getByLabel(label, std::string(instPrefix).append("E"), E);

	for (size_t i = 0; i < Pt->size(); i++)
	{
		TLorentzVector *vec = new TLorentzVector(0., 0., 0., 0.);
		vec->SetPtEtaPhiE(Pt->at(i), Eta->at(i), Phi->at(i), E->at(i));
		out->push_back((*vec));
	}
}

//	std::map<std::pair<std::string, std::string>, std::vector<float>> _data;
float B2GSTop::_get(const edm::Event& iEvent, std::string module, std::string label, size_t index = 0)
{
	diter it = _data.find(dkey(module, label));
	if (it == _data.end())
	{
		edm::Handle<std::vector<float>> item;
		iEvent.getByLabel(module, label, item);
		_data[dkey(module, label)] = *item;
		return item->at(index);
	}
	else
		return it->second.at(index);
}


// ------------ method called once each job just before starting event loop  ------------
void 
B2GSTop::beginJob()
{
}

// ------------ method called once each job just after ending the event loop  ------------
void 
B2GSTop::endJob()
{
}

// ------------ method called when starting to processes a run  ------------
/*
  void 
  B2GSTop::beginRun(edm::Run const&, edm::EventSetup const&)
  {
  }
*/

// ------------ method called when ending the processing of a run  ------------
/*
  void 
  B2GSTop::endRun(edm::Run const&, edm::EventSetup const&)
  {
  }
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
  void 
  B2GSTop::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
  {
  }
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
  void 
  B2GSTop::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
  {
  }
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void
B2GSTop::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
    //The following says we do not know what parameters are allowed so do no validation
    // Please change this to state exactly what you do use, even if it is no parameters
    edm::ParameterSetDescription desc;
    desc.setUnknown();
    descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(B2GSTop);
