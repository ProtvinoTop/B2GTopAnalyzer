import FWCore.ParameterSet.Config as cms

process = cms.Process("Demo")

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 5000

process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        'file:/afs/cern.ch/user/r/razumov/public/B2GEDMNtuple_1.root'
    )
)


#process.load("Analysis.B2GAnaFW.b2gedmntuples_cff")

process.demo = cms.EDAnalyzer('B2GSTop'
)


process.p = cms.Path(process.demo)
